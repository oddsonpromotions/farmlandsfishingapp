<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Beach and Boat</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="touch-icon-ipad-retina.png" />

        <link rel="stylesheet" href="css/main.css?v=1.4">

        <!--[if lt IE 9]>
            <link rel="stylesheet" href="css/ie.css">
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body class="waiting">
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div class="splash">
            <p>February 20-21 2015</p>
        </div>

        <div class="header">
            <img src="img/logo.png" alt="Locallo" title="Locallo">
        </div>

        <div class="main" id="updates-content">
            <div class="post">
                <h4>Weigh-in is finishing in 30 minutes!</h4>

                <span>23 minutes ago</span>

                <p>If you aren't in line before 5pm you won't be able to weigh your fish.</p>
            </div>

            <div class="post">
                <h4>20% off with Century Batteries</h4>

                <span>2 hours ago</span>

                <p>Century is giving 20% off any of there products. Just come down to the marquee to get your voucher.</p>
            </div>

            <div class="post">
                <h4>Ticket sales are close to finishing</h4>

                <span>4 days ago</span>

                <p>Get down to the office soon, because we'll be closing up shop at 7pm.</p>
            </div>
        </div>

        <div class="main" id="deals-content">
            <div class="post">
                <img src="img/kayak.jpg">

                <h4>30% cash back on Ocean Kayaks</h4>

                <p>Purchase an Ocean Kayak and register your kayak on their website and you'll get a 30% cash back!</p>
            </div>

            <div class="post">
                <h4>20% off with Century Batteries</h4>

                <p>Century is giving 20% off any of there products. Just come down to the marquee to get your voucher.</p>
            </div>
        </div>

        <div class="main" id="weather-content">
            <div class="post">
                <h4>Friday</h4>

                <p>A shower or two. Dying southwesterlies.</p>

                <span class="showers"></span>
            </div>

            <div class="post">
                <h4>Saturday</h4>

                <p>Cloudy periods. Westerlies.</p>

                <span class="cloudy"></span>
            </div>

            <div class="post">
                <h4>Sunday</h4>

                <p>Fine spells, shower or two. Westerlies dying out.</p>

                <span class="showers"></span>
            </div>
        </div>

        <div class="main" id="measure-content">
            <div class="post">
                
                <h4>Measure</h4>

                <p>Upload your photos here.</p>
            </div>
        </div>

        <div class="main" id="more-content">
            <div class="post">
                <h4>Free Battery Test</h4>
            </div>
            <div class="post">
                <h4>Bite Times</h4>
            </div>
            <div class="post">
                <h4>Schedule</h4>
            </div>
            <div class="post">
                <h4>Prize List</h4>
            </div>
            <div class="post">
                <h4>Settings</h4>
            </div>
        </div>

        <div class="entry">
            <div class="tab active" id="updates">
                <span class="img"></span>
                <span>Updates</span>
            </div>

            <div class="tab" id="deals">
                <span class="img"></span>
                <span>Deals</span>
            </div>

            <div class="tab" id="weather">
                <span class="img"></span>
                <span>Weather</span>
            </div>

            <div class="tab" id="measure">
                <span class="img"></span>
                <span>Measure</span>
            </div>

            <div class="tab" id="more">
                <span class="img"></span>
                <span>More</span>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script type='application/javascript' src='js/vendor/fastclick.js'></script>

        <script src="js/main.js?v=1.1"></script>
    </body>
</html>