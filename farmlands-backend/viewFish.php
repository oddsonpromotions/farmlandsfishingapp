<?php

	header('Content-type: application/json');

	$db = new mysqli("mysql3.webhost.co.nz","bandbsurvey","davv2009","farmlands");

	$ticket_number = $_GET['t'];

	$res = $db->query("SELECT fish2016.length, fish2016.species FROM fish2016 INNER JOIN entry2016 ON fish2016.entryid=entry2016.entryid WHERE ticket_number = '" . $ticket_number ."' AND fish2016.weight IS NOT NULL");


	$records = array();

	while($row = $res->fetch_assoc() ) {
		switch ($row['species']) {
			case 1:
				$row['species'] = 'Kingfish';
				break;
			case 2:
				$row['species'] = 'John Dory';
				break;
			case 3:
				$row['species'] = 'Kahawai';
				break;
			case 4:
				$row['species'] = 'Gurnard';
				break;
			case 5:
				$row['species'] = 'Snapper';
				break;
			default:
				$row['species'] = 'Trevally';
				break;
		}
		$records[] = $row;
	}

	echo $_GET['jsoncallback'] . '(' . json_encode($records) . ');';

?>