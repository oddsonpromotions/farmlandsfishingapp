//Parse.initialize("U4Yxcwpt013CbI8Ypdefdc10U3DKm9UWvvmvnMqs", "MYIPpOiIIxzVs5gdr4fLM2LwxF18zEW4kiinQWli");
//var Fish = Parse.Object.extend("Fish");

var pointCoords = {};
var currentFish = 0;

var getFish = function () {
	console.log ("getting fish.. ")
	// reset points
	$('.point').remove();
	pointCoords = {};
	$('#length').val('');

	//////////////////////////////////////////////////////////////////
	// var query = new Parse.Query(Fish);
	// query.doesNotExist("length");
	// query.doesNotExist("weird");
	// query.first({
	// 	success: function(object) {
	// 		if (!object) {
	// 			alert("All fish are measured or marked as weird!");
	// 		}
	// 		$('#currentFish').attr('src', object.get('image').url());
	// 		currentFish = object;
	// 	},
	// 	error: function(object, error) {
	// 		alert("Error: " + error.code + " " + error.message);
	// 	}
	// });
	//////////////////////////////////////////////////////////////////

   $.ajax({
        url: 'getFish.php',
        dataType: 'jsonp',
        jsonp: 'jsoncallback',
        timeout: 5000,
        success: function(data, status){       
        	console.log('response received.'); 	
        	console.log(data); 
            	if(data.length==0){
                	alert("All fish are measured or marked as weird!");
                }
                item = data[0];
                $('#currentFish').attr('src', '../uploads/2016/' + item.image);
		 		currentFish = item;
 
        },
        error: function(){
        	console.log('error during ajax get');
            alert('Failed to load. Please try again.');
        }
    });   	
}

$('#getFish').on('click', function() {
	getFish();
});

$('#currentFish').on('click', function(e) {
	pointNo = $('.point').length;

	if(pointNo > 5) {
		// do nothing
	} else if(pointNo > 4) {
		pointCoords[5] = [e.pageY, e.pageX];
		$('#points').append("<span class='point fish' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");

		var furunoBottomWidth = Math.sqrt( Math.pow((pointCoords[0][0] - pointCoords[1][0]), 2) +  Math.pow((pointCoords[0][1] - pointCoords[1][1]), 2) );

		var furunoTopWidth = Math.sqrt( Math.pow((pointCoords[2][0] - pointCoords[3][0]), 2) +  Math.pow((pointCoords[2][1] - pointCoords[3][1]), 2) );

		var fishLength = Math.sqrt( Math.pow((pointCoords[4][0] - pointCoords[5][0]), 2) +  Math.pow((pointCoords[4][1] - pointCoords[5][1]), 2) );

		var difference = furunoBottomWidth - furunoTopWidth;
		var percentageToSubtractFromLength = (difference * 0.78) / furunoBottomWidth * 100;

		console.log(percentageToSubtractFromLength);

		var mmLength = fishLength * (78 / furunoBottomWidth);
		var mmLength = mmLength - (mmLength / 100 * percentageToSubtractFromLength);

		$('#length').val(mmLength);

	} else if(pointNo > 3) {
		pointCoords[4] = [e.pageY, e.pageX];
		$('#points').append("<span class='point fish' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");
	} else if(pointNo > 2) {
		pointCoords[3] = [e.pageY, e.pageX];
		$('#points').append("<span class='point furuno' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");
	} else if(pointNo > 1) {
		pointCoords[2] = [e.pageY, e.pageX];
		$('#points').append("<span class='point furuno' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");
	} else if(pointNo > 0) {
		pointCoords[1] = [e.pageY, e.pageX];
		$('#points').append("<span class='point furuno' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");
	} else {
		pointCoords[0] = [e.pageY, e.pageX];
		$('#points').append("<span class='point furuno' style='top: " + (e.pageY - 4) + "px; left: " + (e.pageX - 2) + "px'></span>");
	}

});

$('#resetPoints').on('click', function() {
	$('.point').remove();
	pointCoords = {};
	$('#length').val('');
});

$("#selectSpecies").change(function(){
	 species = $("#selectSpecies").attr("value");
})


$('#markWeird').on('click', function() {
	var areYouSure = confirm("Are you sure you want to mark this as weird?");

	if (areYouSure) {

		$('#loading').show();

		////////////////////////////////////////////////////////////
		// currentFish.set('weird', true);

		// currentFish.save(null, {
		// 	success: function(object) {
		// 		$('#loading').hide();
		// 		$('.point').remove();
		// 		pointCoords = {};
		// 		$('#length').val('');
		// 		$('#currentFish').attr("src", " ");
		// 		currentFish = 0;
		// 	},
		// 	error: function(object, error) {
		// 		$("#loading").hide();
		// 		alert("Error: " + error.code + " " + error.message);
		// 	}
		// });
		////////////////////////////////////////////////////////////

		console.log('Marking fish as weird.');

	    $.ajax({
	        type: "POST",
	        url: "markAsWeird.php",
	        data: { id: currentFish.id,
	                weird: 1
	        }
	    })
	    .done(function( msg ) {
	    	console.log('Fish marked successfully.');
			$('#loading').hide();
			$('.point').remove();
			pointCoords = {};
			$('#length').val('');
			$('#currentFish').attr("src", " ");
			currentFish = 0;
	    })
	    .fail(function() {
	    	console.log('Error marking fish as weird.');
	    	$("#loading").hide();
	    	alert('Error marking fish as weird. Please try again');
	    });
	}

});

$('#submitLength').on('click', function() {
	$('#loading').show();

	var formattedLength = parseInt($('#length').val());
	formattedLength = formattedLength.toString();

	//////////////////////////////////////////////////////////
	// currentFish.set('length', formattedLength);
	// currentFish.set('remeassured', true);

	// currentFish.save(null, {
	// 	success: function(object) {
	// 		$('#loading').hide();
	// 		$('.point').remove();
	// 		pointCoords = {};
	// 		$('#length').val('');
	// 		$('#currentFish').attr("src", "");
	// 		currentFish = 0;
	// 	},
	// 	error: function(object, error) {
	// 		$("#loading").hide();
	// 		alert("Error: " + error.code + " " + error.message);
	// 	}
	// });
	//////////////////////////////////////////////////////////

	console.log('Submitting fish length.');
	if(species=="0"){
		alert("please choose species");
		$('#loading').hide();
		return;
	}
    $.ajax({
        type: "POST",
        url: "setFishLength.php",
        data: { id: currentFish.id,
                length: formattedLength,
                species:species
        }
    })
    .done(function( msg ) {
    	console.log('Fish length successfully submitted.');
		$('#loading').hide();
		$('.point').remove();
		pointCoords = {};
		$('#length').val('');
		$('#currentFish').attr("src", "");
		currentFish = 0;
		$('#selectSpecies').val("0");
    })
    .fail(function() {
    	console.log('Error setting fish length.');
    	$("#loading").hide();
    	alert('Error setting fish length. Please try again');
    });	


})
