document.ontouchmove = function(event){
    event.preventDefault();
}

$('.splash').delay(3000).fadeOut(200);

window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);

$('#deals').on('click', function() {
  $('.main').hide();
  $('#deals-content').show();
  $('.tab').removeClass('active');
  $(this).addClass('active');
});

$('#updates').on('click', function() {
  $('.main').hide();
  $('#updates-content').show();
  $('.tab').removeClass('active');
  $(this).addClass('active');
});

$('#weather').on('click', function() {
  $('.main').hide();
  $('#weather-content').show();
  $('.tab').removeClass('active');
  $(this).addClass('active');
});

$('#measure').on('click', function() {
  $('.main').hide();
  $('#measure-content').show();
  $('.tab').removeClass('active');
  $(this).addClass('active');
});

$('#more').on('click', function() {
  $('.main').hide();
  $('#more-content').show();
  $('.tab').removeClass('active');
  $(this).addClass('active');
});