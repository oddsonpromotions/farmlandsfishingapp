<?php

	$my_base64_string = $_POST['image'];
	$rand = substr(md5(microtime()),rand(0,26),2);
	$filename = $_POST['ticket'] . '_' . time() . '_' . $rand . '.jpg';

	function base64_to_jpeg( $base64_string, $output_file ) {
	    $ifp = fopen( 'uploads/' . $output_file, "wb" ); 
	    fwrite( $ifp, base64_decode( $base64_string ) ); 
	    fclose( $ifp ); 
	    return( $output_file ); 
	}

	$image = base64_to_jpeg( $my_base64_string, $filename );

	echo "Photo uploaded.";

?>